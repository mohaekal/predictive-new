import React from "react"
import { Row, Col, Card,CardBody,CardTitle,Progress } from "reactstrap"
import TemperatureTable from "./temperatureTable"
import Breadcrumbs from "../../components/@vuexy/breadCrumbs/BreadCrumb"
import EevTable from "./eevTable"
import EevTableHigh from "./eevTable_high"
import VibrationTable from "./vibrationTable"
import DehumTable from "./DehumTable"
import TempAhuTable from "./TempAhuTable"
import DiffAhuTable from "./DiffAhuTable"
import TempBusbarTable from "./TempBusbarTable"
import Loop from "./loop"
import GaugeChart from "react-gauge-chart"
import SalesCard from "./SalesCard"
import "../../assets/scss/pages/dashboard-analytics.scss"
import { temperature } from "chroma-js"
import ExtensionsHeader from "./extensionsHeader"



let $primary = "#7367F0",
  $danger = "#EA5455",
  $warning = "#FF9F43",
  $info = "#00cfe8",
  $primary_light = "#9c8cfc",
  $warning_light = "#FFC085",
  $danger_light = "#f29292",
  $info_light = "#1edec5",
  $stroke_color = "#e8e8e8",
  $label_color = "#e7eef7",
  $white = "#fff"

class Dashboard extends React.Component {
  render() {
    return (
      <React.Fragment>
          <Breadcrumbs
          breadCrumbTitle="Dashboard"
          breadCrumbParent="Assets Overview"
        />
        <Row className="match-height">
          <Col lg="3" md="12">
<TemperatureTable />
          </Col>
          <Col lg="3" md="12">
<EevTable />
          </Col>

          <Col lg="3" md="12">
<EevTableHigh />
          </Col>

          <Col lg="3" md="12">
<VibrationTable />
          </Col>

          <Col lg="3" md="12">
            <DehumTable />
          </Col>

          <Col lg="3" md="12">
            <TempAhuTable />
          </Col>

          <Col lg="3" md="12">
            <DiffAhuTable />
          </Col>

          <Col lg="3" md="12">
            <TempBusbarTable />
          </Col>


          {/* <Col lg="6" md="12">
<SalesCard />

          </Col> */}
          <ExtensionsHeader
          title="Status"
        />
          <Col lg="12" sm="12" className="custom-scroll">

<Loop />

          </Col>





{/* <Col lg="3" md="12" sm="12">

<Loop />
  </Col>      */}


          {/* <Col lg="12" md="12">
<TabNav />
          </Col> */}


        </Row>
      </React.Fragment>
    )
  }
}



export default Dashboard
