import React,{ useState, useEffect } from "react"
import { Link } from "react-router-dom"
import DataTable from "react-data-table-component";
import Spinner from "../.././components/@vuexy/spinner/Fallback-spinner";
import "./custom/dashboard1.css";
import Substring from 'react-substring';
import {Collapse} from 'react-collapse';
import cx from "classnames";




import {
  Card,
  CardFooter,
  Button,
  CardHeader,
  CardTitle,
  Table,
  UncontrolledTooltip,
  Progress
} from "reactstrap"




const CustomLoader = () => (
  <div style={{ padding: '2px' }}>
    <Spinner />
  </div>
);


const columns = [
  {
    name: "Sensor Name",
    selector: "nm",
    sortable: false,
    grow:5,
    wrap : true

  },
  {
    name: "Value",
    selector: "value",
    sortable: false,
    center:true,
    grow:5,
    maxWidth:'10px'


  }
];



function DiffAhuTable(props,index) {

  const [data, setData] = useState([]);
  const [pending, setPending] = React.useState(true);
  const [rows, setRows] = React.useState([]);
  const UpperCaseString = ({ children }) => <span>{children.toUpperCase()}</span>;
const RedString = ({ children }) => <span style={{ color: 'red' }}>{children}</span>;
const GreenString = ({ children }) => <span style={{ color: 'lightgreen' }}>{children}</span>;


  useEffect(() => {

    const interval = setInterval(() => {
      console.log('This will run  every 5 second!');
      const newLocal = "http://10.2.222.217/api/diff.json";
      // const newLocal = "http://kopnaker.com/temperature.json";
      fetch(newLocal)
      .then(res => res.json())
      .then(res => setData(res.rows));
    }, 2000);


      return () => clearInterval(interval);
  }, []);

  React.useEffect(() => {
    const timeout = setTimeout(() => {
      setRows(data);
      setPending(false);
    }, 3000);
    return () => clearTimeout(timeout);
  }, []);


  return (

    <div className="App" >




       <Card className="test">
       <Substring
        substrings={[
            {
                match: new RegExp('JK1', 'gi'),
                component: 'b',
            },
            {
                match: 'custom',
                caseSensitive: true,
                component: RedString,
            },
            {
                match: ['just', 'some', /className/gi],
                props: { style: { backgroundColor: 'red' } },
            },
        ]}
    >
      <DataTable title="Top 5 Differential Pressure AHU" columns={columns} data={data}  pagination paginationPerPage	="5"  striped  defaultSortField="value" defaultSortAsc={false}  progressPending={pending} progressComponent={<CustomLoader />}  />
      </Substring>
      <CardFooter className="text-muted" style={{display: 'flex',  justifyContent:'center', alignItems:'center', }}>
      <p><Link to='/ahu/Differential_Pressure'>See More..</Link></p>
      </CardFooter>
      </Card>

    </div>

  );

};


export default DiffAhuTable





